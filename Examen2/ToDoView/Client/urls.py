from django.urls import path
from Client import views

app_name = 'Client'

urlpatterns = [
    path('', views.Index.as_view(), name='index'),
    path('id_title/', views.ListToDoIDandTitle.as_view(), name='idtitle'),
    path('id_userid/', views.ListToDoIDandUserID.as_view(), name='iduserid'),
    
    path('unsolved/id_title', views.ListToDoIDandTitleUnsolved.as_view(), name='unsolved_idtitle'),
    path('unsolved/id_userid', views.ListToDoIDandUserIDUnsolved.as_view(), name='unsolved_iduserid'),
    
    path('solved/id_title', views.ListToDoIDandTitleSolved.as_view(), name='solved_idtitle'),
    path('solved/id_userid', views.ListToDoIDandUserIDSolved.as_view(), name='solved_iduserid'),
    
    
    path('create/', views.CreateToDoView.as_view(), name='create'),
    path('update/<int:pk>/', views.UpdateToDoView.as_view(), name='update'),
    path('delete/<int:pk>/', views.DeleteToDoView.as_view(), name='delete'),
    path('detail/<int:pk>/', views.DetailToDoView.as_view(), name='detail'),
    
    
    path('register', views.RegisterView.as_view(), name='register'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    
]
