from django.shortcuts import render, redirect
from django.views import generic
from .forms import CreateToDoForm
import requests
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt



#################################################### TO DO ALL ####################################################
class Index(generic.View):
    template_name = 'client/indexid.html'
    url_get = 'http://127.0.0.1:8000/api/todos/ids'
    url_post = 'http://127.0.0.1:8000/api/validate_login/'
    context = {}

    def get(self, request):
        # Verificar si el usuario está autenticado
        if 'username' in request.session and 'validation_number' in request.session:
            self.response = requests.get(url=self.url_get)
            self.context = {"ids": self.response.json()}
            return render(request, self.template_name, self.context)
        else:
            # Si no está autenticado, mostrar el modal de login
            self.context['show_login_modal'] = True
            return render(request, self.template_name, self.context)

    def post(self, request):
        payload = {
            "username": request.POST.get("username"),
            "password": request.POST.get("password"),
        }
        self.response = requests.post(url=self.url_post, data=payload)
        data = self.response.json()

        # Guardar en sesión si el login es exitoso
        if self.response.status_code == 200 and data.get('validation_number'):
            request.session['username'] = data['username']
            request.session['validation_number'] = data['validation_number']
        
        return redirect("Client:index")
    
class ListToDoIDandTitle(generic.View):
    template_name = 'client/indexidtitle.html'
    url_get = 'http://127.0.0.1:8000/api/todos/ids/titles'
    context = {}
    response = None
    def get(self, request):
        self.response = requests.get(url=self.url_get)
        self.context = {
            "idsandtitle":self.response.json()
        }
        return render(request, self.template_name, self.context)
    
class ListToDoIDandUserID(generic.View):
    template_name = 'client/indexiduserid.html'
    url_get = 'http://127.0.0.1:8000/api/todos/ids/userids'
    context = {}
    response = None
    def get(self, request):
        self.response = requests.get(url=self.url_get)
        self.context = {
            "idsanduserid":self.response.json()
        }
        return render(request, self.template_name, self.context)
    
    
#################################################### TO DO UNSOLVED ####################################################
class ListToDoIDandTitleUnsolved(generic.View):
    template_name = 'client/indexidtitle.html'
    url_get = 'http://127.0.0.1:8000/api/todos/unsolved/ids/titles'
    context = {}
    response = None
    def get(self, request):
        self.response = requests.get(url=self.url_get)
        self.context = {
            "idsandtitle":self.response.json()
        }
        return render(request, self.template_name, self.context)


class ListToDoIDandUserIDUnsolved(generic.View):
    template_name = 'client/indexiduserid.html'
    url_get = 'http://127.0.0.1:8000/api/todos/unsolved/ids/userids'
    context = {}
    response = None
    def get(self, request):
        self.response = requests.get(url=self.url_get)
        self.context = {
            "idsanduserid":self.response.json()
        }
        return render(request, self.template_name, self.context)
    
    
#################################################### TO DO SOLVED ####################################################
class ListToDoIDandTitleSolved(generic.View):
    template_name = 'client/indexidtitle.html'
    url_get = 'http://127.0.0.1:8000/api/todos/solved/ids/titles'
    context = {}
    response = None
    def get(self, request):
        self.response = requests.get(url=self.url_get)
        self.context = {
            "idsandtitle":self.response.json()
        }
        return render(request, self.template_name, self.context)
    
    
class ListToDoIDandUserIDSolved(generic.View):
    template_name = 'client/indexiduserid.html'
    url_get = 'http://127.0.0.1:8000/api/todos/solved/ids/userids'
    context = {}
    response = None
    def get(self, request):
        self.response = requests.get(url=self.url_get)
        self.context = {
            "idsanduserid":self.response.json()
        }
        return render(request, self.template_name, self.context)
    
    
#################################################### TO DO CRUD ####################################################
class CreateToDoView(generic.View):
    template_name = 'client/createtodo.html'
    url_post = 'http://127.0.0.1:8000/api/todos/new'
    url_get = 'http://127.0.0.1:8000/api/users'
    context = {}
    form_class = CreateToDoForm
    response = None
    def get(self, request):
        self.response = requests.get(url=self.url_get)
        self.context = {
            "form": self.form_class,
            "idusers":self.response.json()
        }
        return render(request, self.template_name, self.context)
    def post(self, request):
        payload = {
            "title": f"{request.POST.get("title")}",
            "userid": int(request.POST.get("userid")),
            "status": f"{request.POST.get("status")}",
            "description": f"{request.POST.get("description")}",
            "due_date": f"{request.POST.get("due_date")}",
            "priority": f"{request.POST.get("priority")}",
             
            "completed_at": None,
        }
        print(payload)
        self.response = requests.post(url=self.url_post, data=payload)
        return redirect("Client:index")
    
    
class DetailToDoView(generic.View):
    template_name = "client/detailtodo.html"
    context = {}
    url = "http://127.0.0.1:8000/api/todos"
    url2 = "http://127.0.0.1:8000/api/users"
    response = None
    def get(self, request, pk):
        print(pk)
        self.url = self.url + f'/{pk}'
        self.response = requests.get(url=self.url)
        self.response2 = requests.get(url=self.url2)
        self.context = {
            "detailone": self.response.json(),
            "userss": self.response2.json()
        }
        return render(request, self.template_name, self.context)
    
    
class UpdateToDoView(generic.View):
    template_name = 'client/updatetodo.html'
    url_put = 'http://127.0.0.1:8000/api/todos/update/'
    url_get1 = 'http://127.0.0.1:8000/api/users'
    url_get2 = 'http://127.0.0.1:8000/api/todos'
    context = {}
    response = None
    def get(self, request, pk):
        self.url_get2 += f'/{pk}'
        self.response = requests.get(url=self.url_get1)
        print(self.url_get2)
        self.response2 = requests.get(url=self.url_get2)
        self.context = {
            "idusers":self.response.json(),
            "detail":self.response2.json()
        }
        return render(request, self.template_name, self.context)
    def post(self, request, pk):
        payload = {
            "title": request.POST.get("title"),
            "userid": int(request.POST.get("userid")),
            "status": request.POST.get("status"),
            "description": request.POST.get("description"),
            "due_date": request.POST.get("due_date"),
            "priority": request.POST.get("priority"),
            "created_at": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
            "completed_at": None,
        }
        self.url_put += f'{pk}'
        self.response = requests.put(url=self.url_put, json=payload)
        return redirect("Client:index")
    
class DeleteToDoView(generic.View):
    url = 'http://127.0.0.1:8000/api/todos/delete/'
    response = None
    def get(self, request, pk):
        self.url += f'{pk}'
        self.response = requests.delete(url=self.url)
        return redirect("Client:index")
    
    
    
    
#################################################### USER  ####################################################
class RegisterView(generic.View):
    template_name = 'client/register.html'
    url_post = 'http://127.0.0.1:8000/api/users/new'
    context = {}
    
    def get(self, request):
        return render(request, self.template_name, self.context)
    
    @csrf_exempt
    def post(self, request):
        payload = {
            "username": request.POST.get("username"),
            "password": request.POST.get("password"),
            "rol": request.POST.get("rol"),
            "status": request.POST.get("status"),
        }
        self.response = requests.post(url=self.url_post, data=payload)
        return redirect("Client:index")
    
class LogoutView(generic.View):
    def get(self, request):
        request.session.flush()
        return redirect("Client:index")
    
  
 
    
    