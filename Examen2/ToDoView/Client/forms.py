from django import forms



STATUS_CHOICES = [
    ('pending', 'Pending'),
    ('in_progress', 'In Progress'),
    ('completed', 'Completed'),
]
    
PRIORITY_CHOICES = [
    ('low', 'Low'),
    ('medium', 'Medium'),
    ('high', 'High'),
]


class CreateToDoForm(forms.Form):
    title = forms.CharField(label='Title', max_length=64)
    status = forms.CharField(max_length=16, widget= forms.Select(choices=STATUS_CHOICES , attrs={"type":"select", "class":"form-select form-control"}))
    description = forms.CharField( label='Description', max_length=128, widget=forms.Textarea(attrs={ "class": "form-control", "rows": 4, "placeholder": "Enter task description" }))
    due_date = forms.DateTimeField( label='Due Date', widget=forms.DateTimeInput(attrs={ "type": "datetime-local", "class": "form-control", "placeholder": "Select date and time" }))
    priority = forms.CharField(max_length=16, widget= forms.Select(choices=PRIORITY_CHOICES , attrs={"type":"select", "class":"form-select form-control"}))
    


    