
from django.db import models
from django.contrib.auth.models import User




ROL_CHOICES = [
    ('dev', 'Developer'),
    ('viewer', 'Viewer'),
    ('admin', 'Administrator'),
]

STATUS_CHOICES = [
    ('active', 'Active'),
    ('inactive', 'Inactive'),
]

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    username = models.CharField(max_length=64)
    bio = models.CharField(max_length=128, default="No bio provided")
    rol = models.CharField(max_length=20, choices=ROL_CHOICES, default='viewer')
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='inactive')
    password = models.CharField(max_length=128)

    def __str__(self):
        return self.username
    

        
STATUS_CHOICES = [
    ('pending', 'Pending'),
    ('in_progress', 'In Progress'),
    ('completed', 'Completed'),
]
    
PRIORITY_CHOICES = [
    ('low', 'Low'),
    ('medium', 'Medium'),
    ('high', 'High'),
]

class ToDo(models.Model):
    title = models.CharField(max_length=64, default="No title provided.")
    userid = models.ForeignKey(Profile, on_delete=models.CASCADE, default=None)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='pending')
    
    description = models.CharField(max_length=128, default="No description provided.")
    due_date = models.DateTimeField(blank=True, null=True)
    priority = models.CharField(max_length=10, choices=PRIORITY_CHOICES, default='medium')
    created_at = models.DateTimeField(auto_now_add=True)
    completed_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.title
        

