from django.urls import path, include
from Api import views

app_name = 'api'

urlpatterns = [
    
    path('todos', views.ListToDo.as_view(), name='todos'),
    path('todos/new', views.CreateToDoView.as_view(), name='todos_create'),
    path('todos/update/<int:pk>', views.UpdateToDoView.as_view(), name='todos_update'),
    path('todos/delete/<int:pk>', views.DeleteToDoView.as_view(), name='todos_delete'),
    path('todos/<int:pk>', views.DetailToDoView.as_view(), name='todos_detail'),
    
    path('todos/ids', views.ListToDoByID.as_view(), name='todos_ids'),
    
    path('todos/ids/userids', views.ListToDoByIDandUserID.as_view(), name='todos_ids_userids'),
    path('todos/ids/titles', views.ListToDoByIDandTitles.as_view(), name='todos_ids_titles'),
   
    path('todos/unsolved/ids/titles', views.ListToDoUnsolvedByIDandTitles.as_view(), name='todos_ids_titles_unsolved'),
    path('todos/unsolved/ids/userids', views.ListToDoUnsolvedByIDandUserID.as_view(), name='todos_ids_userids_unsolved'),
   
    path('todos/solved/ids/titles', views.ListToDoSolvedByIDandTitles.as_view(), name='todos_ids_titles_solved'),
    path('todos/solved/ids/userids', views.ListToDoSolvedByIDandUserID.as_view(), name='todos_ids_userids_solved'),
    
    path('users', views.ListUser.as_view(), name='users'),
    path('users/new', views.RegisterProfile.as_view(), name='users_create'),
    path('validate_login/', views.LoginValidationView.as_view(), name='validate_login'),
    
]
