from django.contrib import admin
from Api import models

# Register your models here.
@admin.register(models.ToDo)
class ToDoAdmin(admin.ModelAdmin):
    list_display = ('title', 'userid', 'status', 'description', 'due_date', 'priority', 'created_at', 'completed_at')

@admin.register(models.Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'bio', 'rol', 'status','username','password')
