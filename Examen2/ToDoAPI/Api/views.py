from django.shortcuts import render
from rest_framework import views, generics
from rest_framework.response import Response
import hashlib
from .models import Profile
from django.contrib.auth.hashers import make_password
from django.contrib.auth.hashers import check_password
from django.http import JsonResponse
from django.views import View
import random

from .models import ToDo, Profile
from .serializer import ToDoSerializer, ToDoSerializerByID, ToDoSerializerByIDandUserID, ToDoSerializerByIDandTitles, IDusersSerializer, NewProfile, LoginSerializer


##################################################### To Do All ##################################################################
####################################################################################################################################
class ListToDo(generics.ListAPIView):
    serializer_class = ToDoSerializer
    queryset = ToDo.objects.all()
    def get_queryset(self):
        return ToDo.objects.filter()
    

##################################################### To Do Filters ################################################################
####################################################################################################################################
class ListToDoByID(generics.ListAPIView):
    serializer_class = ToDoSerializerByID
    queryset = ToDo.objects.all()
    def get_queryset(self):
        return ToDo.objects.filter()
    
class ListToDoByIDandTitles(generics.ListAPIView):
    serializer_class = ToDoSerializerByIDandTitles
    queryset = ToDo.objects.all()
    def get_queryset(self):
        return ToDo.objects.filter()   
    
class ListToDoByIDandUserID(generics.ListAPIView):
    serializer_class = ToDoSerializerByIDandUserID
    queryset = ToDo.objects.all()
    def get_queryset(self):
        return ToDo.objects.filter()
    

    
##################################################### To Do Unsolved filters #######################################################
####################################################################################################################################
class ListToDoUnsolvedByIDandTitles(generics.ListAPIView):
    serializer_class = ToDoSerializerByIDandTitles
    queryset = ToDo.objects.all()
    def get_queryset(self):
        return ToDo.objects.filter(status='pending')

class ListToDoUnsolvedByIDandUserID(generics.ListAPIView):
    serializer_class = ToDoSerializerByIDandUserID
    queryset = ToDo.objects.all()
    def get_queryset(self):
        return ToDo.objects.filter(status='pending')

#################################################### To Do Solved filters ##########################################################
####################################################################################################################################
class ListToDoSolvedByIDandTitles(generics.ListAPIView):
    serializer_class = ToDoSerializerByIDandTitles
    queryset = ToDo.objects.all()
    def get_queryset(self):
        return ToDo.objects.filter(status='completed')

class ListToDoSolvedByIDandUserID(generics.ListAPIView):
    serializer_class = ToDoSerializerByIDandUserID
    queryset = ToDo.objects.all()
    def get_queryset(self):
        return ToDo.objects.filter(status='completed')
    
#################################################### To Do C_RD_U_D filters ##########################################################
####################################################################################################################################
class DetailToDoView(generics.RetrieveAPIView):
    def get(self,request, pk):
        queryset = ToDo.objects.get(pk=pk)
        data = ToDoSerializer(queryset).data
        return Response(data)

class CreateToDoView(generics.CreateAPIView):
    serializer_class = ToDoSerializer
    queryset = ToDo.objects.all()

class UpdateToDoView(generics.UpdateAPIView):
    serializer_class = ToDoSerializer
    queryset = ToDo.objects.all()
    
class DeleteToDoView(generics.DestroyAPIView):
    serializer_class = ToDoSerializer
    queryset = ToDo.objects.all()
    
    
#################################################### INFO USER ##########################################################
class ListUser(generics.ListAPIView):
    serializer_class = IDusersSerializer
    queryset = Profile.objects.all()
    def get_queryset(self):
        return Profile.objects.filter()
    


class RegisterProfile(generics.CreateAPIView):
    serializer_class = NewProfile
    queryset = Profile.objects.all()

    def perform_create(self, serializer):
        # Hash de la contraseña antes de guardarla
        password = serializer.validated_data['password']
        hashed_password = make_password(password)
        serializer.validated_data['password'] = hashed_password

        serializer.save()

class LoginValidationView(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            username = serializer.validated_data['username']
            password = serializer.validated_data['password']
        try:
            profile = Profile.objects.get(username=username)
            if check_password(password, profile.password):
                validation_number = random.randint(100000, 999999)
                response_data = {
                    'username': profile.username,
                    'validation_number': validation_number
                }
                return JsonResponse(response_data, status=200)
            else:
                return JsonResponse({'username': username, 'validation_number': None}, status=400)
        except Profile.DoesNotExist:
            return JsonResponse({'error': 'Invalid username or password'}, status=400)
        
    
    
    
    
    


    
    
    
    

    
    

    