from Api.models import ToDo, Profile, User
from rest_framework import serializers

class ToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ["id", "title", "userid", "status", "description", "due_date", "priority", "created_at", "completed_at"]

class ToDoSerializerByID(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ["id","status"]
        
class ToDoSerializerByIDandUserID(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ["id", "userid", "status"]
        
class ToDoSerializerByIDandTitles(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ["id", "title", "status"]
        
        
        
class IDusersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ["id", "username"]
        
class NewProfile(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True) 
    class Meta:
        model = Profile
        fields = ['username', 'bio', 'rol', 'status', 'password']
        

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

        
        

        
        


